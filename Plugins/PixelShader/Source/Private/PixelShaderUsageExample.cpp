/******************************************************************************
* The MIT License (MIT)
*
* Copyright (c) 2015 Fredrik Lindh
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
******************************************************************************/

#include "PixelShaderPrivatePCH.h"
#include "RHIStaticStates.h"

//It seems to be the convention to expose all vertex declarations as globals, and then reference them as externs in the headers where they are needed.
//It kind of makes sense since they do not contain any parameters that change and are purely used as their names suggest, as declarations :)
TGlobalResource<PlanetVertexDeclaration> GPlanetVertexDeclaration;

FPixelShaderUsageExample::FPixelShaderUsageExample(ERHIFeatureLevel::Type ShaderFeatureLevel)
{
	FeatureLevel = ShaderFeatureLevel;

	Constants = PlanetShaderConstants();
	//Constants.Color = FVector4(1.0f, 1.0f, 1.0f, 1.0f);
	Variables = PlanetShaderVariables();
	
	bIsExecuting = false;
	bIsUnloading = false;

	CurrentTexture = NULL;
	CurrentRenderTarget = NULL;

	// Init planet patch

#define Square(x) ((x)*(x))

	const int patch_size_in_verts = 30; // configurable
	const int patch_size_in_quads = patch_size_in_verts - 1;
	const int skirt_verts = 4 * patch_size_in_verts;
	const int verts_total = Square(patch_size_in_verts) + skirt_verts;
	const int indices_per_reset = 2;
	const int indices_per_strip =
		2 + patch_size_in_quads * 2 + indices_per_reset;
	const int skirt_indices = patch_size_in_quads * 4 + 2 * indices_per_strip;
	const int indices_total =
		patch_size_in_quads*indices_per_strip - indices_per_reset + skirt_indices;

#undef Square

#define V3(x, y, z) PlanetVertex({ FVector(x, y, z) })

	PlanetVertex *verts = new PlanetVertex[verts_total];
	{
		int n = 0;

		double div = 1.0 / patch_size_in_quads;

		for (int x = 0; x < patch_size_in_verts; ++x)
			verts[n++] = V3(x*div, 0.0f, 1.0f); // skirt

		for (int y = 0; y < patch_size_in_verts; ++y)
		{
			verts[n++] = V3(0.0f, y*div, 1.0f); // skirt

			for (int x = 0; x < patch_size_in_verts; ++x)
				verts[n++] = V3(x*div, y*div, 0.0f);

			verts[n++] = V3(1.0f, y*div, 1.0f); // skirt
		}

		for (int x = 0; x < patch_size_in_verts; ++x)
			verts[n++] = V3(x*div, 1.0f, 1.0f); // skirt

		//assert(n == verts_total);
	}

	uint32_t *indices = new uint32_t[indices_total];
	{
		int n = 0;

		uint32_t v0 = 0;
		uint32_t v1 = patch_size_in_verts + 1;

		// skirt
		for (int x = 0; x < patch_size_in_verts; ++x)
		{
			indices[n++] = v0++;
			indices[n++] = v1++;
		}

		// reset
		indices[n++] = v1 - 1;
		indices[n++] = v0;
		v1++;

		for (int y = 0; y < patch_size_in_quads; ++y)
		{
			for (int x = 0; x < patch_size_in_verts + 2; ++x)
			{
				indices[n++] = v0++;
				indices[n++] = v1++;
			}
			if (y + 1 < patch_size_in_quads)
			{
				// reset
				indices[n++] = v1 - 1;
				indices[n++] = v0;
			}
		}

		v0++;
		// reset
		indices[n++] = v1 - 1;
		indices[n++] = v0;

		// skirt
		for (int x = 0; x < patch_size_in_verts; ++x)
		{
			indices[n++] = v0++;
			indices[n++] = v1++;
		}

		//assert(n == indices_total);
	}

	Vertices = verts;
	Indices = indices;
	NumVertices = verts_total;
	NumIndices = indices_total;

	ThePatches = NULL;
}

FPixelShaderUsageExample::~FPixelShaderUsageExample()
{
	bIsUnloading = true;

	delete[] Vertices;
	delete[] Indices;
	delete[] ThePatches;
}

void FPixelShaderUsageExample::ExecutePixelShader(UTextureRenderTarget2D* RenderTarget,
	const FMatrix &Projection, const FMatrix &ViewRotation, int PatchCount, PlanetPatch *Patches)
{
	if (bIsUnloading || bIsExecuting) //Skip this execution round if we are already executing
	{
		return;
	}

	bIsExecuting = true;

	NumPatches = PatchCount;
	delete[] ThePatches;
	ThePatches = new PlanetPatch[PatchCount];
	memcpy(ThePatches, Patches, sizeof(PlanetPatch)*PatchCount);

	Variables.ViewRotation = ViewRotation;
	Variables.Projection = Projection;

	CurrentRenderTarget = RenderTarget;

	//This macro sends the function we declare inside to be run on the render thread. What we do is essentially just send this class and tell the render thread to run the internal render function as soon as it can.
	//I am still not 100% Certain on the thread safety of this, if you are getting crashes, depending on how advanced code you have in the start of the ExecutePixelShader function, you might have to use a lock :)
	ENQUEUE_UNIQUE_RENDER_COMMAND_ONEPARAMETER(
		FPixelShaderRunner,
		FPixelShaderUsageExample*, PixelShader, this,
		{
			PixelShader->ExecutePixelShaderInternal();
		}
	);
}

void FPixelShaderUsageExample::ExecutePixelShaderInternal()
{
	check(IsInRenderingThread());

	if (bIsUnloading)
	{
		return;
	}

	FRHICommandListImmediate& RHICmdList = GRHICommandList.GetImmediateCommandList();

	CurrentTexture = CurrentRenderTarget->GetRenderTargetResource()->GetRenderTargetTexture();
	SetRenderTarget(RHICmdList, CurrentTexture, FTextureRHIRef());
	RHICmdList.SetBlendState(TStaticBlendState<>::GetRHI());
	RHICmdList.SetRasterizerState(TStaticRasterizerState<>::GetRHI());
	RHICmdList.SetDepthStencilState(TStaticDepthStencilState<false, CF_LessEqual>::GetRHI());
	RHICmdList.SetRasterizerState(TStaticRasterizerState<FM_Solid, CM_CCW>::GetRHI());
	RHICmdList.Clear(true, FLinearColor(0.0f, 0.0f, 0.0f), true, 0.0f, false, 0, FIntRect(-1000, -1000, 1000, 1000));
	
	static FGlobalBoundShaderState BoundShaderState;
	TShaderMapRef<PlanetVertexShader> VertexShader(GetGlobalShaderMap(FeatureLevel));
	TShaderMapRef<PlanetPixelShader> PixelShader(GetGlobalShaderMap(FeatureLevel));

	SetGlobalBoundShaderState(RHICmdList, FeatureLevel, BoundShaderState, GPlanetVertexDeclaration.VertexDeclarationRHI, *VertexShader, *PixelShader);

	for (int i = 0; i < NumPatches; i++)
	{
		Variables.Corner0 = ThePatches[i].Corners[0].Position;
		Variables.Corner1 = ThePatches[i].Corners[1].Position;
		Variables.Corner2 = ThePatches[i].Corners[2].Position;
		Variables.Corner3 = ThePatches[i].Corners[3].Position;
		Variables.Normal0 = ThePatches[i].Corners[0].Normal;
		Variables.Normal1 = ThePatches[i].Corners[1].Normal;
		Variables.Normal2 = ThePatches[i].Corners[2].Normal;
		Variables.Normal3 = ThePatches[i].Corners[3].Normal;

		VertexShader->SetUniformBuffers(RHICmdList, Constants, Variables);
		PixelShader->SetUniformBuffers(RHICmdList, Constants, Variables);

		DrawIndexedPrimitiveUP(RHICmdList, PT_TriangleStrip, 0, NumVertices, NumIndices - 2, Indices, sizeof(Indices[0]), Vertices, sizeof(Vertices[0]));
	}

	bIsExecuting = false;
}
