/******************************************************************************
* The MIT License (MIT)
*
* Copyright (c) 2015 Fredrik Lindh
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
******************************************************************************/

#pragma once

#include "GlobalShader.h"
#include "UniformBuffer.h"
#include "RHICommandList.h"

BEGIN_UNIFORM_BUFFER_STRUCT(PlanetShaderConstants, )
END_UNIFORM_BUFFER_STRUCT(PlanetShaderConstants)
typedef TUniformBufferRef<PlanetShaderConstants> PlanetShaderConstantsRef;

BEGIN_UNIFORM_BUFFER_STRUCT(PlanetShaderVariables, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, Corner0)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, Corner1)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, Corner2)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, Corner3)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, Normal0)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, Normal1)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, Normal2)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, Normal3)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FMatrix, ViewRotation)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FMatrix, Projection)
END_UNIFORM_BUFFER_STRUCT(PlanetShaderVariables)
typedef TUniformBufferRef<PlanetShaderVariables> PlanetShaderVariablesRef;

struct PlanetVertex
{
	FVector UV; // xy = uv, z = skirt scale
};

class PlanetVertexDeclaration : public FRenderResource
{
public:
	FVertexDeclarationRHIRef VertexDeclarationRHI;

	virtual void InitRHI() override
	{
		FVertexDeclarationElementList Elements;
		uint32 Stride = sizeof(PlanetVertex);
		Elements.Add(FVertexElement(0, STRUCT_OFFSET(PlanetVertex, UV), VET_Float3, 0, Stride));
		VertexDeclarationRHI = RHICreateVertexDeclaration(Elements);
	}

	virtual void ReleaseRHI() override
	{
		VertexDeclarationRHI.SafeRelease();
	}
};

class PlanetVertexShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(PlanetVertexShader, Global);
public:

	PlanetVertexShader() {}

	explicit PlanetVertexShader(const ShaderMetaType::CompiledShaderInitializerType& Initializer) :
		FGlobalShader(Initializer) {}

	static bool ShouldCache(EShaderPlatform Platform) { return IsFeatureLevelSupported(Platform, ERHIFeatureLevel::SM5); }

	void SetUniformBuffers(FRHICommandList& RHICmdList, PlanetShaderConstants& Constants, PlanetShaderVariables& Variables);
};


class PlanetPixelShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(PlanetPixelShader, Global);

public:

	PlanetPixelShader() {}

	explicit PlanetPixelShader(const ShaderMetaType::CompiledShaderInitializerType& Initializer)
		: FGlobalShader(Initializer) {}

	static bool ShouldCache(EShaderPlatform Platform) { return IsFeatureLevelSupported(Platform, ERHIFeatureLevel::SM5); }

	void SetUniformBuffers(FRHICommandList& RHICmdList, PlanetShaderConstants& Constants, PlanetShaderVariables& Variables);
};

