/******************************************************************************
* The MIT License (MIT)
*
* Copyright (c) 2015 Fredrik Lindh
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Private/PixelShaderDeclaration.h"

struct PlanetPatchCorner
{
	FVector Position;
	FVector Normal;

	static inline PlanetPatchCorner FromPositionAndRadius(FVector Position, float Radius)
	{
		PlanetPatchCorner Result;
		Result.Normal = Position.GetUnsafeNormal();
		Result.Position = Result.Normal * Radius;
		return Result;
	}
};

struct PlanetPatch
{
	PlanetPatchCorner Corners[4];
};

class PIXELSHADER_API FPixelShaderUsageExample
{
public:
	FPixelShaderUsageExample(ERHIFeatureLevel::Type ShaderFeatureLevel);
	~FPixelShaderUsageExample();

	/********************************************************************************************************/
	/* Let the user change rendertarget during runtime if they want to :D                                   */
	/* @param RenderTarget - This is the output rendertarget!                                               */
	/********************************************************************************************************/
	void ExecutePixelShader(UTextureRenderTarget2D* RenderTarget,
		const FMatrix &Projection, const FMatrix &ViewRotation, int PatchCount, PlanetPatch *Patches);

	/************************************************************************/
	/* Only execute this from the render thread!!!                          */
	/************************************************************************/
	void ExecutePixelShaderInternal();

private:
	bool bIsExecuting;
	bool bIsUnloading;

	// Planet patch geometry
	PlanetVertex *Vertices;
	uint32 *Indices;
	uint32 NumVertices;
	uint32 NumIndices;

	int NumPatches;
	PlanetPatch *ThePatches;

	PlanetShaderConstants Constants;
	PlanetShaderVariables Variables;
	ERHIFeatureLevel::Type FeatureLevel;

	/** Main texture */
	FTexture2DRHIRef CurrentTexture;
	UTextureRenderTarget2D* CurrentRenderTarget;
};
