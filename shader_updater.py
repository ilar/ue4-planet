import os
import glob
import shutil
import time

source_file_pattern = 'Shaders/*.usf'
dest_path = 'D:/Ohjelmat/Epic Games/UE_4.13/Engine/Shaders/'

old_times_by_fname = {}

while 1:
    for fname in glob.glob(source_file_pattern):
        mod_time = os.stat(fname).st_mtime
        if not fname in old_times_by_fname or old_times_by_fname[fname] != mod_time:
            old_times_by_fname[fname] = mod_time
            shutil.copy(fname, dest_path)
            print("Copied \"" + fname + "\"")
    time.sleep(1)
