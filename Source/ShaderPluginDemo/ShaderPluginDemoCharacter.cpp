// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ShaderPluginDemo.h"
#include "ShaderPluginDemoCharacter.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AShaderPluginDemoCharacter

AShaderPluginDemoCharacter::AShaderPluginDemoCharacter() {
    // Set size for collision capsule
    GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

    // Create a CameraComponent
    FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>
                                 (TEXT("FirstPersonCamera"));
    FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
    FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
    FirstPersonCameraComponent->bUsePawnControlRotation = true;

    TotalElapsedTime = 0;

	FVector StartPos = FVector(-5.0, 5.0, -10.0);
	FVector Forward = FVector(0.5, -0.5, 1.0).GetSafeNormal();
	FVector Right = FVector::CrossProduct(FVector(0.0, 1.0, 0.0), Forward).GetSafeNormal();
	FVector Up = FVector::CrossProduct(Forward, Right).GetSafeNormal();

	//UE_LOG(LogTemp, Warning, TEXT("x: %f, %f, %f"), Right.X, Right.Y, Right.Z);
	//UE_LOG(LogTemp, Warning, TEXT("y: %f, %f, %f"), Up.X, Up.Y, Up.Z);
	//UE_LOG(LogTemp, Warning, TEXT("z: %f, %f, %f"), Forward.X, Forward.Y, Forward.Z);

	Camera.Position = StartPos;
	Camera.Rotation.SetIdentity();
	Camera.Rotation.SetAxes(&Right, &Up, &Forward, NULL);
	Camera.Projection = FPerspectiveMatrix(45.0f, 1.0f, 1.0f, 0.1f);
}

//Since we need the featurelevel, we need to create the shaders from beginplay, and not in the ctor.
void AShaderPluginDemoCharacter::BeginPlay() {
    Super::BeginPlay();
    
	PixelShading = new FPixelShaderUsageExample(GetWorld()->Scene->GetFeatureLevel());
}

void AShaderPluginDemoCharacter::BeginDestroy() {
    Super::BeginDestroy();

    delete PixelShading;
}

void AShaderPluginDemoCharacter::Tick(float DeltaSeconds) {
    Super::Tick(DeltaSeconds);

    TotalElapsedTime += DeltaSeconds;

	float t = sin(TotalElapsedTime)*0.5f + 0.5f;
	FVector StartPos = FVector(-2.0f, 4.0f, -5.0f);
	FVector EndPos = FVector(-1.0f, 1.0f, -1.0f);
	FVector Pos = StartPos*(1.0f - t) + EndPos*t;

	Camera.Position = Pos;

    if (PixelShading) {
        FTexture2DRHIRef InputTexture = NULL;

		//APlayerController *PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		//ULocalPlayer *LocalPlayer = PlayerController->GetLocalPlayer();
		//FSceneViewProjectionData ProjectionData;
		//LocalPlayer->GetProjectionData(NULL, eSSP_FULL, ProjectionData);
		//FMatrix Projection = ProjectionData.ProjectionMatrix;
		
		float Radius = 1.0f;

#define PLANET_CORNER(x, y, z) PlanetPatchCorner::FromPositionAndRadius(FVector(x, y, z), Radius)

		const int PatchCount = 6;
		PlanetPatch Patches[PatchCount] = {
			{ { PLANET_CORNER(-1, -1, -1), PLANET_CORNER( 1, -1, -1), PLANET_CORNER(-1,  1, -1), PLANET_CORNER( 1,  1, -1) } },
			{ { PLANET_CORNER( 1, -1, -1), PLANET_CORNER( 1, -1,  1), PLANET_CORNER( 1,  1, -1), PLANET_CORNER( 1,  1,  1) } },
			{ { PLANET_CORNER( 1, -1,  1), PLANET_CORNER(-1, -1,  1), PLANET_CORNER( 1,  1,  1), PLANET_CORNER(-1,  1,  1) } },
			{ { PLANET_CORNER(-1, -1,  1), PLANET_CORNER(-1, -1, -1), PLANET_CORNER(-1,  1,  1), PLANET_CORNER(-1,  1, -1) } },
			{ { PLANET_CORNER(-1,  1, -1), PLANET_CORNER( 1,  1, -1), PLANET_CORNER(-1,  1,  1), PLANET_CORNER( 1,  1,  1) } },
			{ { PLANET_CORNER(-1, -1,  1), PLANET_CORNER( 1, -1,  1), PLANET_CORNER(-1, -1, -1), PLANET_CORNER( 1, -1, -1) } }
		};

		for (int i = 0; i < PatchCount; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				Patches[i].Corners[j].Position -= Camera.Position;
			}
		}

		PixelShading->ExecutePixelShader(RenderTarget, Camera.Projection, Camera.Rotation.Inverse(), PatchCount, Patches);
    }
}

void AShaderPluginDemoCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent_) {
    check(InputComponent_);

    //ShaderPluginDemo Specific input mappings
    InputComponent_->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
    InputComponent_->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

    InputComponent_->BindAxis("MoveForward", this,
                             &AShaderPluginDemoCharacter::MoveForward);
    InputComponent_->BindAxis("MoveRight", this,
                             &AShaderPluginDemoCharacter::MoveRight);

    InputComponent_->BindAxis("Turn", this, &APawn::AddControllerYawInput);
    InputComponent_->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}

void AShaderPluginDemoCharacter::MoveForward(float Value) {
    if (Value != 0.0f) {
        // add movement in that direction
        AddMovementInput(GetActorForwardVector(), Value);
    }
}

void AShaderPluginDemoCharacter::MoveRight(float Value) {
    if (Value != 0.0f) {
        // add movement in that direction
        AddMovementInput(GetActorRightVector(), Value);
    }
}
