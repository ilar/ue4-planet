// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "PixelShaderUsageExample.h"
#include "ShaderPluginDemoCharacter.generated.h"

class UInputComponent;

UCLASS(config=Game)
class AShaderPluginDemoCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;
public:
	AShaderPluginDemoCharacter();

	virtual void BeginPlay();

protected:

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ShaderDemo)
		UTextureRenderTarget2D* RenderTarget;

protected:
	virtual void BeginDestroy() override;
	virtual void Tick(float DeltaSeconds) override;

private:
	FPixelShaderUsageExample* PixelShading;

	float TotalElapsedTime;


	struct camera_t
	{
		FVector Position;
		FMatrix Rotation;
		FMatrix Projection;
	};

	camera_t Camera;
};

